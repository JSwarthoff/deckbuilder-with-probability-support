﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;

namespace Algorytmy
{
    public partial class Form1 : Form
    {
        public List<string> cards;
        IList<Absorb> baza = new List<Absorb>(); //miejsce gdzie zaladowane sa wszystkie kart
        IList<Absorb> landsbaza = new List<Absorb>(); //tu sa wszystkie landy
        IList<RootObject> targetcards = new List<RootObject>(); //a tu wszyskie karty poddane analizie
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string json = "", path = Path.GetFullPath("json.txt"); /*"C:\\Users\\faustveritas07\\source\\repos\\algorytmy\\Algorytmy\\json.txt"*/

            json = File.ReadAllText(path);

            JObject search = JObject.Parse(json);
            IList<JToken> results = search["Karta"].Children().ToList();

            for (int i = 0; results.Count > i; i++)
            {
                int a = results[i].ToString().IndexOf(":");
                string gone = results[i].ToString().Remove(0, a + 1);
                gone = gone.Remove(gone.Length - 1);

                baza.Add(JsonConvert.DeserializeObject<Absorb>(gone + "}"));
            }
            for(int i = 0; baza.Count > i; i++)
            {
                if (baza[i].type.Contains("Land") == true)
                {
                    landsbaza.Add(baza[i]);
                }
            }
            results.Clear();
            checkBoxOnThePlay.Checked = true;
            checkBoxOnThePlay.Text = "Yes";
            comboBoxMulliganStrategy.SelectedIndex = 2;
            checkedListBoxMulliganLands.SetItemChecked(0,true);
            checkedListBoxMulliganLands.SetItemChecked(1, true);
            checkedListBoxMulliganLands.SetItemChecked(6, true);
            checkedListBoxMulliganLands.SetItemChecked(7, true);
        }

     
        private void button1_Click(object sender, EventArgs e) //mozna wyrzucic
        {
            
            getCards();
           
        }

        private void getCards()
        {
            string path = Directory.GetCurrentDirectory()+"\\karty.txt";
            string data = File.ReadAllText(path);
            int a = 0;
            string name, count;
            string[] parsed = data.Split('\n');

            for(int i = 0; parsed.Length > i; i++)
            {
                a=parsed[i].IndexOf(" ");
                name = parsed[i].Substring(a+1);
                name = name.Remove(name.Length - 1);
                count = parsed[i].Remove(a);
                RootObject karta = new RootObject();
                karta.Absorb = findCard(name);
                karta.ilosc = Int32.Parse(count);
                if (karta.Absorb.name != null) // przy przerzucaniu tego trzeba dodać warunek do listinga
                    targetcards.Add(karta);
            }
            parsed[0].IndexOf(" ");
            
        }

        private Absorb findCard(string name)
        {
            Absorb found = new Absorb();
            for(int i = 0; baza.Count > i; i++)
            {
                if(baza[i].name.Contains(name)==true)
                found = baza[i];
            }
            //f (found.name == null)
                //MessageBox.Show("brak karty "+name+" w standardzie");
            return found;
        }
        private void buttonUpload_Click(object sender, EventArgs e)
        {
            //otwieranie pliku txt
            using (OpenFileDialog ofd = new OpenFileDialog()
            {
                Filter = "Text Documents(*.txt|*.txt",
                ValidateNames = true,
                Multiselect = false,
                CheckFileExists = true,
            }
                )

                //dodawanie do listboxow
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    string name, count;
                    //string[] parsed = data.Split('\n');
                    string[] lines = System.IO.File.ReadAllLines(ofd.FileName);
                    int a = 0;
                    //tu potrzebny for z getCards
                    for (int i = 0; lines.Length > i; i++)
                    {
                        a = lines[i].IndexOf(" ");
                        name = lines[i].Substring(a + 1);
                        name = name.Remove(name.Length - 1);
                        count = lines[i].Remove(a);
                        RootObject karta = new RootObject();
                        karta.Absorb = findCard(name);
                        karta.ilosc = Int32.Parse(count);
                        if (karta.Absorb.name != null) // przy przerzucaniu tego trzeba dodać warunek do listinga
                        {
                           
                            targetcards.Add(karta);
                            if (karta.Absorb.type.Contains("Land") == true)
                            {
                                listBoxLands.Items.Add(karta.ilosc + " " + karta.Absorb.name);
                                dataGridViewCards.Rows.Add(karta.ilosc.ToString(), karta.Absorb.name, karta.Absorb.manaCost,0,0,0);
         
                            }
                            else
                            {
                                listBoxNonlands.Items.Add(karta.ilosc + " " + karta.Absorb.name);
                                dataGridViewCards.Rows.Add(karta.ilosc.ToString(), karta.Absorb.name, karta.Absorb.manaCost,0,0,0);
                            }
                            
                            
                        }
                            
                    }
                }
        }
        public bool checkIfsixty()
        {
            const string caption = "Error!";
            int suma = 0;
            foreach (RootObject cos in targetcards)
            {
                suma = suma+cos.ilosc;
            }
            if (suma == 60)
                return true;
            else
            {
                MessageBox.Show("Check if you have all the cards from Standard format!", caption);
                return false;
            }
                

        }
        public IList<Absorb> prepereDeck()
        {
            IList<Absorb> goodDeck = new List<Absorb>();
            if (checkIfsixty() == true)
            {
                foreach(RootObject cos in targetcards)
                {
                    for(int i = 0; i < cos.ilosc; i++)
                    {
                        goodDeck.Add(cos.Absorb);
                    }
                }
            }
            return goodDeck;
        }
        public double getHighestCost()
        {
            double cost = 0;
            foreach(RootObject cos in targetcards)
            {
                if (cost < cos.Absorb.convertedManaCost)
                {
                    cost = cos.Absorb.convertedManaCost;
                }
            }
            return cost;
        }
        public int countLands(IList<Absorb> hand)
        {
            int cos = 0;
            foreach(Absorb nic in hand)
            {
                if (nic.type.Contains("Land") == true)
                    cos++;
            }
            return cos;
        }
        public Hand countLandsColor(IList<Absorb> hand)
        {
            int[] cosRob = { 0, 0, 0, 0, 0, 0 };
            Hand land = new Hand();
            foreach (Absorb nic in hand)
            {
                if (nic.type.Contains("Land") == true)
                {
                    land.ilosc++;
                    cosRob=getColors(nic.colorIdentity);
                    land.barwa[0] = land.barwa[0] + cosRob[0];
                    land.barwa[1] = land.barwa[1] + cosRob[1];
                    land.barwa[2] = land.barwa[2] + cosRob[2];
                    land.barwa[3] = land.barwa[3] + cosRob[3];
                    land.barwa[4] = land.barwa[4] + cosRob[4];
                    land.barwa[5] = land.barwa[5] + cosRob[5];
                }  
            }
            return land;
        }
        public int [] getColors(List<string> kolor)
        {
            int[] cos = { 0, 0, 0, 0, 0, 0 };
            foreach (string s in kolor)
            {
                if (s.Equals("B"))
                    cos[0]++;
                else if (s.Equals("R"))
                    cos[1]++;
                else if (s.Equals("G"))
                    cos[2]++;
                else if (s.Equals("U"))
                    cos[3]++;
                else if (s.Equals("W"))
                    cos[4]++;
                else cos[5]++;
            }
                return cos;
        }
        public int[] getColorsC(string kolor)
        {
            int[] cos = { 0, 0, 0, 0, 0, 0 };
            string[] parsed={"s","a" };
            if (kolor != null)
            {
                parsed = kolor.Split('}');
                for (int i=0;i<parsed.Length-1;i++)
                {
                    if (parsed[i].Contains("B"))
                        cos[0]++;
                    else if (parsed[i].Contains("R"))
                        cos[1]++;
                    else if (parsed[i].Contains("G"))
                        cos[2]++;
                    else if (parsed[i].Contains("U"))
                        cos[3]++;
                    else if (parsed[i].Contains("W"))
                        cos[4]++;
                    else cos[5]++;
                }
            }
            return cos;
        }
        public IList<Absorb> createHand(IList<Absorb> goodDeck,int mull,Random rand)
        {
            int  a;
            IList<Absorb> hand = new List<Absorb>();
            for (int i = 0; i < 7; i++)
            {
                a = rand.Next(0, 60 - i);
                hand.Add(goodDeck[a]);
                goodDeck.Remove(goodDeck[a]);
            }
            
            if (mull != 0)
            {
                a = countLands(hand);
                if (a<4)//eksperymentalnie dobrane
                {
                    foreach (Absorb nic in hand)
                    {
                        if (nic.type.Contains("Land") == false)
                        {
                            goodDeck.Add(nic);
                            hand.Remove(nic);
                            break;
                        }

                    } 
                }
                else
                {
                    foreach (Absorb nic in hand)
                    {
                        if (nic.type.Contains("Land") == true)
                        {
                            goodDeck.Add(nic);
                            hand.Remove(nic);
                            break;
                        }

                    }
                }

            }
            return hand;
        }
        public int getMuliganStrategy()
        {
            return comboBoxMulliganStrategy.SelectedIndex;
        }
        public int getTargetNumberOfLandsUp()
        {
            List<int> zbior = new List<int>();
            foreach(string s in checkedListBoxMulliganLands.CheckedItems)
            {
                zbior.Add(Int32.Parse(s));
            }
            for(int i = 0; i < zbior.Count; i++)
            {
                if (zbior[i] + 1 != zbior[i + 1])
                    return zbior[i + 1];
            }
            return 0;
        }
        public int getTargetNumberOfLandsDown()
        {
            List<int> zbior = new List<int>();
            foreach (string s in checkedListBoxMulliganLands.CheckedItems)
            {
                zbior.Add(Int32.Parse(s));
            }
            for (int i = 0; i < zbior.Count; i++)
            {
                if (zbior[i] + 1 != zbior[i + 1])
                    return zbior[i];
            }
            return 0;
        }
        
        public void fillTableC(int[,] array, int[,] arrayCmc,  int[,] arrayPlay, int x, int y, Hand mana)
        {
            int[] cos = { 0, 0, 0, 0, 0, 0 };
            for (int i = 0; i < x; i++)
            {
                if (targetcards[i].Absorb.convertedManaCost <= mana.ilosc)
                {
                    arrayCmc[i, y] = 1;
                    cos = getColorsC(targetcards[i].Absorb.manaCost);
                    if (mana.barwa[0] >= cos[0] && mana.barwa[1] >= cos[1] && mana.barwa[2] >= cos[2] && mana.barwa[3] >= cos[3] && mana.barwa[4] >= cos[4])
                    {
                        array[i, y] = 1;
                    }
                    else
                    {
                        array[i, y] = 0;
                        arrayCmc[i, y] = 0;
                        arrayPlay[i, y ]=0;
                    }
                }
                else
                {
                    array[i, y] = 0;
                    arrayCmc[i, y] = 0;
                    arrayPlay[i, y] = 0;
                }
            }
        }
        public void fillTableTurnC(int[,] array, int[,] arrayCmc, int[,] arrayPlay, int x, int y, Hand mana, int tura, IList<Absorb> hand)
        {
            int[] cos = { 0, 0, 0, 0, 0, 0 };
            for (int i = 0; i < x; i++)
            {
                if (targetcards[i].Absorb.convertedManaCost == tura && tura <= mana.ilosc)//powinno byc == i <= convertedManaCost <= mana.ilosc && tura <= mana.ilosc
                {
                    arrayCmc[i, y] = 1;
                    cos = getColorsC(targetcards[i].Absorb.manaCost);/*tutaj trzeba zrobic ifowisko sprawdzajace kolory*/
                    if (mana.barwa[0] >= cos[0] && mana.barwa[1] >= cos[1] && mana.barwa[2] >= cos[2] && mana.barwa[3] >= cos[3] && mana.barwa[4] >= cos[4])
                    {
                        array[i, y] = 1;
                        if (hand.Contains(targetcards[i].Absorb))
                            arrayPlay[i, y] = 1;
                    }
                }
            }
            
        }
        public void countTable(int[,] tab, int liczbaSym,double [] wynik)
        {
            for(int i=0;i<targetcards.Count;i++)
                wynik[i]=0;
            for(int i=0;i<targetcards.Count;i++)
            {
                for(int j=0;j<liczbaSym;j++)
                {
                    wynik[i]=wynik[i]+tab[i,j];
                }
                

            }
            
        }
        public void countTableCmc(int[,] tab, int liczbaSym, double[] wynik,double [] drugi)
        {
            // int [] wynik = new int[targetcards.Count];
            for (int i = 0; i < targetcards.Count; i++)
                wynik[i] = 0;
            for (int i = 0; i < targetcards.Count; i++)
            {
                for (int j = 0; j < liczbaSym; j++)
                {
                    wynik[i] = wynik[i] + tab[i, j];
                }
            }
            for (int i = 0; i < targetcards.Count; i++)
                wynik[i] = drugi[i] / wynik[i];///drugi[i];
        }
        public int getPlayOrDraw()
        {
            if (checkBoxOnThePlay.Checked == true)
            {
                return 1;
            }
            else
                return 0;
        }
        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            
            Random rand = new Random();
            RootObject karta = new RootObject();
            int a, playOrDraw=getPlayOrDraw(), targetNumberOfLandsDown = getTargetNumberOfLandsDown(), mulliganStrategy = getMuliganStrategy();
            int targetNumberOfLandsUp = getTargetNumberOfLandsUp();
            int liczbaSymulacji = 100000;///bedzie potrzebne 100000 symulacji 
            int[,] array = new int[targetcards.Count, liczbaSymulacji];
            int[,] arrayCmc = new int[targetcards.Count, liczbaSymulacji];
            int[,] arrayPlay = new int[targetcards.Count, liczbaSymulacji];
            double[] Pmana = new double[targetcards.Count];
            double[] PmanaCmc = new double[targetcards.Count];
            double[] PmanaPlay = new double[targetcards.Count];
            targetcards =targetcards.OrderBy(o => o.Absorb.convertedManaCost).ToList();
            Hand landAndColor = new Hand();
            Hand landAndColorDraw = new Hand();
            IList<Absorb> hand = new List<Absorb>();
            IList<Absorb> goodDeck = new List<Absorb>();
            goodDeck = prepereDeck();
            double biggestManaCost = getHighestCost();
            for (int j = 0; j < liczbaSymulacji; j++)
            {
                a = 0;
                do {
                    goodDeck = prepereDeck();
                    goodDeck = goodDeck.OrderBy(x => Guid.NewGuid()).ToList();//bardzo potrzebne
                    hand.Clear();
                    hand = createHand(goodDeck,a,rand);
                    landAndColor=countLandsColor(hand);
                    a++;
                } while ((targetNumberOfLandsDown >=landAndColor.ilosc  || landAndColor.ilosc>=targetNumberOfLandsUp) && a!=mulliganStrategy+1);
                fillTableC(array,arrayCmc,arrayPlay, targetcards.Count, j, landAndColor);
                    if(playOrDraw==1)
                fillTableTurnC(array, arrayCmc,arrayPlay, targetcards.Count, j, landAndColor, 1,hand);
                for (int i = 1+playOrDraw; i <= biggestManaCost; i++)//2 to play 1 to draw
                {
                    a = rand.Next(0, goodDeck.Count - i);
                    hand.Add(goodDeck[a]);
                    goodDeck.Remove(goodDeck[a]);
                    landAndColorDraw = countLandsColor(hand);
                    fillTableTurnC(array,arrayCmc,arrayPlay, targetcards.Count, j, landAndColorDraw, i,hand);
                }
                hand.Clear();
            }
            countTable(array,liczbaSymulacji,Pmana);
            countTableCmc(arrayCmc, liczbaSymulacji, PmanaCmc,Pmana);
            countTable(arrayPlay, liczbaSymulacji, PmanaPlay);/*z tego miejsca mozesz wpisac do tabeli*/
            //wpisywanie wynikow do tablicy
            dataGridViewCards.Refresh();

            //wypisywanie prawdopodobienstwa do tablicy
            for(int i = 0; i<dataGridViewCards.RowCount - 1;i++)
            {
                dataGridViewCards.Rows[i].Cells[3].Value = PmanaCmc[i];
                dataGridViewCards.Rows[i].Cells[4].Value = Pmana[i]/100000;
                dataGridViewCards.Rows[i].Cells[5].Value = PmanaPlay[i]/100000;
            }

            dataGridView1.Rows[rowIndex].Cells[columnIndex].Style.BackColor = Color.Red;




            //for (int i = 0; i < targetcards.Count; i++)
            //{
            //    dataGridViewCards.Rows[i].Add(karta.ilosc.ToString(), karta.Absorb.name, karta.Absorb.manaCost, PmanaCmc, Pmana, PmanaPlay);
            //}
            //dataGridViewCards.Refresh();

        }

        private void checkBoxOnThePlay_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxOnThePlay.Checked)
            {
                checkBoxOnThePlay.Text = "Yes";
                checkBoxOnThePlay.Checked = true;
            }
            else
            {
                checkBoxOnThePlay.Text = "No";
                checkBoxOnThePlay.Checked = false;
            }
        }
    }
}