﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy
{

    public class ForeignData
    {
        public string flavorText { get; set; }
        public string language { get; set; }
        public string name { get; set; }
        public string text { get; set; }
        public string type { get; set; }
    }

    public class Legalities
    {
        public string brawl { get; set; }
        public string commander { get; set; }
        public string duel { get; set; }
        public string future { get; set; }
        public string historic { get; set; }
        public string legacy { get; set; }
        public string modern { get; set; }
        public string pioneer { get; set; }
        public string standard { get; set; }
        public string vintage { get; set; }
    }

    public class PurchaseUrls
    {
        public string cardmarket { get; set; }
        public string mtgstocks { get; set; }
        public string tcgplayer { get; set; }
    }

    public class Ruling
    {
        public string date { get; set; }
        public string text { get; set; }
    }

    public class Absorb
    {
        public Absorb() { }
        public List<string> colorIdentity { get; set; }
        public List<string> colors { get; set; }
        public double convertedManaCost { get; set; }
        public int edhrecRank { get; set; }
        public List<ForeignData> foreignData { get; set; }
        public string layout { get; set; }
        public Legalities legalities { get; set; }
        public string manaCost { get; set; }
        public int mtgArenaId { get; set; }
        public int mtgoId { get; set; }
        public int mtgstocksId { get; set; }
        public string name { get; set; }
        public List<string> printings { get; set; }
        public PurchaseUrls purchaseUrls { get; set; }
        public List<Ruling> rulings { get; set; }
        public string scryfallOracleId { get; set; }
        public List<object> subtypes { get; set; }
        public List<object> supertypes { get; set; }
        public string text { get; set; }
        public string type { get; set; }
        public List<string> types { get; set; }
        public string uuid { get; set; }
    }

    public class RootObject
    {
        public RootObject() { }
        public int ilosc { get; set; }
        public Absorb Absorb { get; set; }
    }
}
