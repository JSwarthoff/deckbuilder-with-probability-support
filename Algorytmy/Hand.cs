﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorytmy
{
    public class Hand
    {
        public int ilosc;
        public int[] barwa = { 0, 0, 0, 0, 0, 0 };
        public Hand()
        {
            ilosc = 0;
            
        }
        public static Hand operator+(Hand one, Hand two)
        {
            Hand result = new Hand();
            result.ilosc = one.ilosc + two.ilosc;
            result.barwa[0] = one.barwa[0] + two.barwa[0];
            result.barwa[1] = one.barwa[1] + two.barwa[1];
            result.barwa[2] = one.barwa[2] + two.barwa[2];
            result.barwa[3] = one.barwa[3] + two.barwa[3];
            result.barwa[4] = one.barwa[4] + two.barwa[4];
            result.barwa[5] = one.barwa[5] + two.barwa[5];
            return result;
        }
    }
}
