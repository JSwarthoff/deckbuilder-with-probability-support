﻿namespace Algorytmy
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageCards = new System.Windows.Forms.TabPage();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.dataGridViewCards = new System.Windows.Forms.DataGridView();
            this.listBoxLands = new System.Windows.Forms.ListBox();
            this.listBoxNonlands = new System.Windows.Forms.ListBox();
            this.labelLands = new System.Windows.Forms.Label();
            this.labelNonlands = new System.Windows.Forms.Label();
            this.tabPageCustomize = new System.Windows.Forms.TabPage();
            this.checkBoxOnThePlay = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labelMulliganLands = new System.Windows.Forms.Label();
            this.checkedListBoxMulliganLands = new System.Windows.Forms.CheckedListBox();
            this.labelMulliganStrategy = new System.Windows.Forms.Label();
            this.comboBoxMulliganStrategy = new System.Windows.Forms.ComboBox();
            this.tabPageGraphs = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPageInfo = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Col1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl.SuspendLayout();
            this.tabPageCards.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCards)).BeginInit();
            this.tabPageCustomize.SuspendLayout();
            this.tabPageGraphs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabPageInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageCards);
            this.tabControl.Controls.Add(this.tabPageCustomize);
            this.tabControl.Controls.Add(this.tabPageGraphs);
            this.tabControl.Controls.Add(this.tabPageInfo);
            this.tabControl.ImageList = this.imageList;
            this.tabControl.Location = new System.Drawing.Point(12, 41);
            this.tabControl.Name = "tabControl";
            this.tabControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(926, 536);
            this.tabControl.TabIndex = 1;
            // 
            // tabPageCards
            // 
            this.tabPageCards.Controls.Add(this.buttonUpload);
            this.tabPageCards.Controls.Add(this.dataGridViewCards);
            this.tabPageCards.Controls.Add(this.listBoxLands);
            this.tabPageCards.Controls.Add(this.listBoxNonlands);
            this.tabPageCards.Controls.Add(this.labelLands);
            this.tabPageCards.Controls.Add(this.labelNonlands);
            this.tabPageCards.ImageIndex = 0;
            this.tabPageCards.Location = new System.Drawing.Point(4, 39);
            this.tabPageCards.Name = "tabPageCards";
            this.tabPageCards.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCards.Size = new System.Drawing.Size(918, 493);
            this.tabPageCards.TabIndex = 0;
            this.tabPageCards.Text = "Cards";
            this.tabPageCards.UseVisualStyleBackColor = true;
            // 
            // buttonUpload
            // 
            this.buttonUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonUpload.Location = new System.Drawing.Point(6, 450);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(906, 37);
            this.buttonUpload.TabIndex = 4;
            this.buttonUpload.Text = "Upload deck from txt file";
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // dataGridViewCards
            // 
            this.dataGridViewCards.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCards.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col1,
            this.Col2,
            this.Col3,
            this.Col4,
            this.Col5,
            this.Col6});
            this.dataGridViewCards.Location = new System.Drawing.Point(268, 19);
            this.dataGridViewCards.Name = "dataGridViewCards";
            this.dataGridViewCards.Size = new System.Drawing.Size(644, 425);
            this.dataGridViewCards.TabIndex = 4;
            // 
            // listBoxLands
            // 
            this.listBoxLands.FormattingEnabled = true;
            this.listBoxLands.Location = new System.Drawing.Point(6, 270);
            this.listBoxLands.Name = "listBoxLands";
            this.listBoxLands.Size = new System.Drawing.Size(256, 173);
            this.listBoxLands.TabIndex = 3;
            // 
            // listBoxNonlands
            // 
            this.listBoxNonlands.FormattingEnabled = true;
            this.listBoxNonlands.Location = new System.Drawing.Point(6, 19);
            this.listBoxNonlands.Name = "listBoxNonlands";
            this.listBoxNonlands.Size = new System.Drawing.Size(256, 225);
            this.listBoxNonlands.TabIndex = 2;
            // 
            // labelLands
            // 
            this.labelLands.AutoSize = true;
            this.labelLands.Location = new System.Drawing.Point(3, 252);
            this.labelLands.Name = "labelLands";
            this.labelLands.Size = new System.Drawing.Size(39, 13);
            this.labelLands.TabIndex = 1;
            this.labelLands.Text = "Lands:";
            // 
            // labelNonlands
            // 
            this.labelNonlands.AutoSize = true;
            this.labelNonlands.Location = new System.Drawing.Point(2, 3);
            this.labelNonlands.Name = "labelNonlands";
            this.labelNonlands.Size = new System.Drawing.Size(55, 13);
            this.labelNonlands.TabIndex = 0;
            this.labelNonlands.Text = "Nonlands:";
            // 
            // tabPageCustomize
            // 
            this.tabPageCustomize.Controls.Add(this.checkBoxOnThePlay);
            this.tabPageCustomize.Controls.Add(this.label3);
            this.tabPageCustomize.Controls.Add(this.labelMulliganLands);
            this.tabPageCustomize.Controls.Add(this.checkedListBoxMulliganLands);
            this.tabPageCustomize.Controls.Add(this.labelMulliganStrategy);
            this.tabPageCustomize.Controls.Add(this.comboBoxMulliganStrategy);
            this.tabPageCustomize.ImageIndex = 1;
            this.tabPageCustomize.Location = new System.Drawing.Point(4, 39);
            this.tabPageCustomize.Name = "tabPageCustomize";
            this.tabPageCustomize.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCustomize.Size = new System.Drawing.Size(918, 493);
            this.tabPageCustomize.TabIndex = 1;
            this.tabPageCustomize.Text = "Customize";
            this.tabPageCustomize.UseVisualStyleBackColor = true;
            // 
            // checkBoxOnThePlay
            // 
            this.checkBoxOnThePlay.Appearance = System.Windows.Forms.Appearance.Button;
            this.checkBoxOnThePlay.Checked = true;
            this.checkBoxOnThePlay.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxOnThePlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBoxOnThePlay.Location = new System.Drawing.Point(586, 66);
            this.checkBoxOnThePlay.Name = "checkBoxOnThePlay";
            this.checkBoxOnThePlay.Size = new System.Drawing.Size(212, 50);
            this.checkBoxOnThePlay.TabIndex = 5;
            this.checkBoxOnThePlay.UseVisualStyleBackColor = true;
            this.checkBoxOnThePlay.CheckedChanged += new System.EventHandler(this.checkBoxOnThePlay_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(579, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 42);
            this.label3.TabIndex = 4;
            this.label3.Text = "On the play:";
            // 
            // labelMulliganLands
            // 
            this.labelMulliganLands.AutoSize = true;
            this.labelMulliganLands.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMulliganLands.Location = new System.Drawing.Point(6, 127);
            this.labelMulliganLands.Name = "labelMulliganLands";
            this.labelMulliganLands.Size = new System.Drawing.Size(543, 42);
            this.labelMulliganLands.TabIndex = 3;
            this.labelMulliganLands.Text = "Mulligan on X lands - choose X:";
            // 
            // checkedListBoxMulliganLands
            // 
            this.checkedListBoxMulliganLands.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkedListBoxMulliganLands.FormattingEnabled = true;
            this.checkedListBoxMulliganLands.Items.AddRange(new object[] {
            "0 ",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.checkedListBoxMulliganLands.Location = new System.Drawing.Point(13, 172);
            this.checkedListBoxMulliganLands.Name = "checkedListBoxMulliganLands";
            this.checkedListBoxMulliganLands.Size = new System.Drawing.Size(558, 301);
            this.checkedListBoxMulliganLands.TabIndex = 2;
            // 
            // labelMulliganStrategy
            // 
            this.labelMulliganStrategy.AutoSize = true;
            this.labelMulliganStrategy.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMulliganStrategy.Location = new System.Drawing.Point(6, 21);
            this.labelMulliganStrategy.Name = "labelMulliganStrategy";
            this.labelMulliganStrategy.Size = new System.Drawing.Size(306, 42);
            this.labelMulliganStrategy.TabIndex = 1;
            this.labelMulliganStrategy.Text = "Mulligan Strategy";
            // 
            // comboBoxMulliganStrategy
            // 
            this.comboBoxMulliganStrategy.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBoxMulliganStrategy.FormattingEnabled = true;
            this.comboBoxMulliganStrategy.Items.AddRange(new object[] {
            "Never Mulligan",
            "Mulligan down to 6 cards",
            "Mulligan down to 5 cards",
            "Mulligan down to 4 cards",
            "Mulligan down to 3 cards",
            "Mulligan down to 2 cards",
            "Mulligan down to 1 card",
            "Mulligan down to 0 cards"});
            this.comboBoxMulliganStrategy.Location = new System.Drawing.Point(13, 66);
            this.comboBoxMulliganStrategy.Name = "comboBoxMulliganStrategy";
            this.comboBoxMulliganStrategy.Size = new System.Drawing.Size(558, 50);
            this.comboBoxMulliganStrategy.TabIndex = 0;
            // 
            // tabPageGraphs
            // 
            this.tabPageGraphs.Controls.Add(this.button2);
            this.tabPageGraphs.Controls.Add(this.chart2);
            this.tabPageGraphs.Controls.Add(this.chart1);
            this.tabPageGraphs.ImageIndex = 2;
            this.tabPageGraphs.Location = new System.Drawing.Point(4, 39);
            this.tabPageGraphs.Name = "tabPageGraphs";
            this.tabPageGraphs.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGraphs.Size = new System.Drawing.Size(918, 493);
            this.tabPageGraphs.TabIndex = 2;
            this.tabPageGraphs.Text = "Graphs";
            this.tabPageGraphs.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(20, 49);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // chart2
            // 
            chartArea1.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart2.Legends.Add(legend1);
            this.chart2.Location = new System.Drawing.Point(6, 196);
            this.chart2.Name = "chart2";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart2.Series.Add(series1);
            this.chart2.Size = new System.Drawing.Size(415, 270);
            this.chart2.TabIndex = 1;
            this.chart2.Text = "chart2";
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(444, 196);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(416, 270);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // tabPageInfo
            // 
            this.tabPageInfo.Controls.Add(this.pictureBox3);
            this.tabPageInfo.Controls.Add(this.pictureBox2);
            this.tabPageInfo.Controls.Add(this.pictureBox1);
            this.tabPageInfo.ImageIndex = 3;
            this.tabPageInfo.Location = new System.Drawing.Point(4, 39);
            this.tabPageInfo.Name = "tabPageInfo";
            this.tabPageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInfo.Size = new System.Drawing.Size(918, 493);
            this.tabPageInfo.TabIndex = 3;
            this.tabPageInfo.Text = "Info";
            this.tabPageInfo.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Algorytmy.Properties.Resources.bar_chart;
            this.pictureBox3.Location = new System.Drawing.Point(57, 295);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 113);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Algorytmy.Properties.Resources.settings;
            this.pictureBox2.Location = new System.Drawing.Point(57, 176);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 113);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Algorytmy.Properties.Resources.vocabulary;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(57, 57);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 113);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "vocabulary.png");
            this.imageList.Images.SetKeyName(1, "settings.png");
            this.imageList.Images.SetKeyName(2, "bar-chart.png");
            this.imageList.Images.SetKeyName(3, "information.png");
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonCalculate.Location = new System.Drawing.Point(680, 12);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(254, 62);
            this.buttonCalculate.TabIndex = 3;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(367, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "Simulations:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(571, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 29);
            this.label4.TabIndex = 6;
            this.label4.Text = "0/0";
            // 
            // Col1
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Col1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Col1.HeaderText = "Count";
            this.Col1.Name = "Col1";
            // 
            // Col2
            // 
            this.Col2.HeaderText = "Card Name";
            this.Col2.Name = "Col2";
            // 
            // Col3
            // 
            this.Col3.HeaderText = "Mana Cost";
            this.Col3.Name = "Col3";
            // 
            // Col4
            // 
            this.Col4.HeaderText = "Pmana|cmc";
            this.Col4.Name = "Col4";
            // 
            // Col5
            // 
            this.Col5.HeaderText = "Pmana";
            this.Col5.Name = "Col5";
            // 
            // Col6
            // 
            this.Col6.HeaderText = "Pplay";
            this.Col6.Name = "Col6";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 589);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "MTG ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageCards.ResumeLayout(false);
            this.tabPageCards.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCards)).EndInit();
            this.tabPageCustomize.ResumeLayout(false);
            this.tabPageCustomize.PerformLayout();
            this.tabPageGraphs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabPageInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageCards;
        private System.Windows.Forms.ListBox listBoxLands;
        private System.Windows.Forms.ListBox listBoxNonlands;
        private System.Windows.Forms.Label labelLands;
        private System.Windows.Forms.Label labelNonlands;
        private System.Windows.Forms.TabPage tabPageCustomize;
        private System.Windows.Forms.TabPage tabPageGraphs;
        private System.Windows.Forms.TabPage tabPageInfo;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.DataGridView dataGridViewCards;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.CheckBox checkBoxOnThePlay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelMulliganLands;
        private System.Windows.Forms.CheckedListBox checkedListBoxMulliganLands;
        private System.Windows.Forms.Label labelMulliganStrategy;
        private System.Windows.Forms.ComboBox comboBoxMulliganStrategy;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col6;
    }
}

